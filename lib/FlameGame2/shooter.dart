import 'dart:ui';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'dart:math';
import 'package:flutter/gestures.dart';
import 'package:navigation_flame/data/data.dart';

class MyGame extends Game {
  Size screenSize;
  double tileSize;

  Data data;
  Rect bgRect;
  MyGame(this.data)
  {
    initialiaze();
  }

  void initialiaze() async
  {
    resize(await Flame.util.initialDimensions());
    bgRect = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
  
  }


  void render(Canvas canvas) 
  {
    
    Paint bgPaint = Paint();
    Paint backBPaint = Paint();
    bgPaint.color = Color(0xff576574);
    backBPaint.color = Color(0xffFFF00);
    canvas.drawRect(bgRect, bgPaint); 
    

  }

  void update(double t) 
  {
    
  }

  void resize(Size size) 
  {
    screenSize = size;
    tileSize = screenSize.width / 9;
  }


  void onTapDown(TapDownDetails d)
  {
     
  }
}
