
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'GeoLocator/geo.dart';
import 'States/splachScreen.dart';
import 'data/data.dart';
import "FlameFolder/myGame.dart";
import "data/dataInFile.dart";


void main() async
{
  Data data;
  data = new Data();
  
  WidgetsFlutterBinding.ensureInitialized();
  await data.appFlameUtil.fullScreen();
  await data.appFlameUtil.setOrientation(DeviceOrientation.portraitUp);
  runApp(MyApp( data));

  //SystemChrome.setEnabledSystemUIOverlays([]);
  
}

class MyApp extends StatelessWidget {
  Data myData;

  MyApp( Data data)
  {
    myData = data;
  }
  
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Proceedit',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LocationState(myData));  
  }
}

