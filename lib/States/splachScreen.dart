import 'package:flutter/material.dart';
import 'package:navigation_flame/States/mainMenu.dart';
import 'package:navigation_flame/data/data.dart';
import 'package:splashscreen/splashscreen.dart';


class SplashScreenProceedit extends StatefulWidget 
{
  Data data;
  SplashScreenProceedit(this.data) 
  {

  } 
    @override
    MySplashScreen createState() => MySplashScreen(data);
      
}
class MySplashScreen extends State<SplashScreenProceedit> {
  Data data;
  MySplashScreen(this.data)
  {
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: new SplashScreen(    
          seconds: 1,
          navigateAfterSeconds: new MainMenu(data),
          image: new Image.asset('lib/assets/images/LogoTransparent.png'),
          backgroundColor: Colors.white,
          photoSize: data.size.height/3.5,
          loaderColor: Colors.blue
          ),
        )
      ],
    );
}   
}