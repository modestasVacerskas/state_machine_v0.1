
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:navigation_flame/GeoLocator/geo.dart';
import 'package:navigation_flame/GeoLocator/locationData/userLocation.dart';
import 'package:navigation_flame/States/optionsState.dart';
import 'package:navigation_flame/data/data.dart';
import 'package:navigation_flame/data/dataInFile.dart';
import 'package:navigation_flame/data/dataInJson.dart';
import 'package:provider/provider.dart';
import '../FlameFolder/myGame.dart';

 class MainMenu extends StatefulWidget 
 {
  Data allData;
  
  MainMenu(Data data)
  {
    allData = data;

  }
  
  @override
  MainMenuPage createState() => MainMenuPage(allData);
}

class MainMenuPage extends State<MainMenu> 
{
  final myController = TextEditingController();
  Data data;

  LocationService loc;
  LocationData locData;
  String fData = "No Data";
  MyGame game;
  DataInJson json;
  
  MainMenuPage(Data allData)
  {
  data = allData;
  loc = LocationService();
  game = new MyGame(data);
  json =  DataInJson();
  }

  @override
  Widget build(BuildContext context) {
    
    var userLocation = Provider.of<UserLocation>(context);
     if(userLocation == null)
    {
      print("NO LCOATION SHOWED");
      FileData.readFormFile().then((fDataS)
      {
        setState(()
        
      {
        if(fDataS.length < 1)
        {
          fDataS = "No lcoation available";
           fData = fDataS.toString();
        }
        else
        {
  
        fData ="Old location:" + fDataS.toString();
        data.locInString = fDataS.toString();
           
        }
     
      });
      });
    }
    else
    {
    
    fData = "Current location:" + userLocation.latitude.toString() + " " + userLocation.longitude.toString();
    data.locInString = fData;
    data.dataInJsonFile.jsonStruct.lastLatitude =  userLocation.latitude.toString();
    data.dataInJsonFile.jsonStruct.lastLongitude =  userLocation.longitude.toString();
    data.dataInJsonFile.jsonStruct.curretnLatitude =  userLocation.latitude.toString();
    data.dataInJsonFile.jsonStruct.currentLongtitude =  userLocation.longitude.toString();
    }
  
    return Scaffold(
  
      appBar: AppBar(
        title: Text('Main Menu'),
      ),
      body: Center
      (child: Column
      (
        children: <Widget>[
          
          new RaisedButton
          (
            child:  Text('Go to Second Screen'),
            onPressed: () 
          {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => OptionsState(data)),
            );
          },
        ),
         new RaisedButton
        (
          child: Text('launch Game'),
          onPressed: () {
         
            game.resumeEngine();
            data.tapper.onTapDown = game.onTapDown;
            data.appFlameUtil.addGestureRecognizer(data.tapper);
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MyGame(data).widget),
            );
          },
        ),
        
        new Text(fData),
        ],
      ),
  
      ),
      );
 
    
  }
}

