import 'package:flutter/material.dart';
import 'package:navigation_flame/States/mainMenu.dart';
import 'package:navigation_flame/data/data.dart';


class OptionsState extends StatefulWidget 
{
  Data data;
  OptionsState(this.data)
  {
  }
  @override
  _MyOptionsState createState() => _MyOptionsState(data);
}

class _MyOptionsState  extends State<OptionsState> {
  Data data;
  _MyOptionsState(this.data)
  {
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Options Menu'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Go back to Home Screen'),
          onPressed: () {
            Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => MainMenu(data)));
            
          },
        ),
      ),
    );
  }
}
 