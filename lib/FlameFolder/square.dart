import 'dart:ui';
import 'myGame.dart';
import 'dart:math';

class Square
{
  final MyGame game;
  Rect square;
  Paint squareCollor; 
  int squareCollorNumber; 
  bool cliked = false;
  bool scored = false;
  int tempX;
  int tempY;
  Random randomColor;

  bool canBePlaced = false;

  Square(this.game, int postitionX, int postitionY)
  {
    tempX  = postitionX;
    tempY = postitionY;
    randomColor = Random();
    squareCollorNumber =  randomColor.nextInt(4);
    squareCollor = Paint();
    changeSquareCollor();
    square = Rect.fromLTWH(postitionX.toDouble() *  game.tileSize , postitionY.toDouble() * game.tileSize, game.tileSize, game.tileSize);
  }
   
  void render(Canvas c) 
  {
    
    c.drawRect(square, squareCollor);
  }

  void update(double t) 
  {
    changeSquareColorCallable(squareCollorNumber);
    changeSquareCollor();

  }

  void onTapDown() 
  {
    cliked = true;
    print("Screen tapped");
    if(game.gui.currentLevel == squareCollorNumber)
    {
      //gui add points
      print("point added");
      squareCollor.color = Color(0xffFFFFFF);
  
    }
    else
    {
        //gui minus health
      print("health taken");
      squareCollor.color = Color(0xff000000);
       
    }
  }

  
  void changeSquareCollor()
  {
   
      switch(squareCollorNumber)
      {
      case 0:
       squareCollor.color  = Color(0xff0047FF);
       break;
      case 1:
        squareCollor.color = Color(0xffff0000);
        break;
      case 2:
        squareCollor.color = Color(0xffffFF00);
        break; 
      case 3:
        squareCollor.color = Color(0xffff00FF);
        break;
      case 4:
        squareCollor.color = Color(0xffFFFFFF);
        break;
      default:
        squareCollor.color = Color(0xff000000);
        break;
      }

  }
  void changeSquareColorCallable(int colorNumber)
  {   
    squareCollorNumber = colorNumber;
  }
}