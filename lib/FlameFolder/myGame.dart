import 'dart:ui';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'dart:math';
import 'package:flutter/gestures.dart';
import 'package:navigation_flame/FlameFolder/SquareGird.dart';
import 'package:navigation_flame/data/data.dart';
import 'square.dart';
import "topFile.dart";


class MyGame extends Game {
  Size screenSize;
  double tileSize;
  GUI gui;
  SquareGrid table;
  Random rnd;
  Data data;
  Rect bgRect;
  Rect backButton;
  MyGame(this.data)
  {
    initialiaze();
  }

  void initialiaze() async
  {
    resize(await Flame.util.initialDimensions());
    gui = GUI();
    rnd = Random();
    table = SquareGrid(this);
    gui.initializeTimer();
    bgRect = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    backButton = Rect.fromLTWH(4.5.toDouble() * tileSize , 12.toDouble() * tileSize, tileSize, tileSize);
  
  }


  void render(Canvas canvas) 
  {
    
    Paint bgPaint = Paint();
    Paint backBPaint = Paint();
    bgPaint.color = Color(0xff576574);
    backBPaint.color = Color(0xffFFF00);
    canvas.drawRect(bgRect, bgPaint); 
    
    table.render(canvas);
    canvas.drawRect(backButton, backBPaint);
  }

  void update(double t) 
  {
    gui.countingTime();
    if(gui.changeColor == true)
    {
      if(gui.currentLevel ==  1)
      {
        int randomNumber = rnd.nextInt(5);      

        for(int i = 0; i < randomNumber; i++)
        {
          int randomRow = rnd.nextInt(9);
          int randomCollum = rnd.nextInt(9);
          int randomColor =  rnd.nextInt(5);
          table.randomColorForRandomSquare(randomRow, randomCollum, randomColor);        
          gui.changeColor = false;
        }       
      }
    }
    table.update(t);
  }

  void resize(Size size) 
  {
    screenSize = size;
    tileSize = screenSize.width / 9;
  }


  void onTapDown(TapDownDetails d)
  {
     table.onTapDown(d);
     
     if(backButton.contains(d.globalPosition))
     {
      //TODO: suspend the engine
     }
     
  }
}