

import 'package:flame/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:navigation_flame/FlameFolder/myGame.dart';
import 'package:navigation_flame/GeoLocator/locationData/userLocation.dart';
import 'package:navigation_flame/data/dataInJson.dart';
import "dataInFile.dart";
import "package:navigation_flame/GeoLocator/geo.dart";

class Data
{
  BuildContext context;
  Util appFlameUtil;
  TapGestureRecognizer tapper;
  Size size;
  UserLocation loc;
  String locInString;
  LocationService locServ;
  String lasLocationInFile;
  bool isGameStateLunched = false;
  Color mainMenuColor; 
  DataInJson dataInJsonFile;

  Data()
  {
    dataInitialize();
  }

  void dataInitialize() async
  {
    appFlameUtil = Util();
    tapper = TapGestureRecognizer();
    loc = UserLocation();
    size = await appFlameUtil.initialDimensions();
    locInString  = LocationService().getLocation().toString();
    dataInJsonFile = new DataInJson();

  }
 
 
}