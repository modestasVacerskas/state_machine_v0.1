import 'dart:io';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart';

import 'package:path_provider/path_provider.dart';

class DataInJson
{
  String dataJSON;
  var jsonResult;
  JsonStruct jsonStruct;

  DataInJson()
  {
    loadJson();
  }

  loadJson() async 
  {
  dataJSON = await rootBundle.loadString('lib/data/dataInJson/data.json');
  jsonResult = json.decode(dataJSON);
  jsonStruct = new JsonStruct.fromJson(jsonResult);
  print(jsonStruct.currentLongtitude);
  }
  
}

class JsonStruct
{
  String  curretnLatitude;
  String currentLongtitude;
  String lastLatitude;
  String lastLongitude;

  JsonStruct({this.curretnLatitude,
    this.currentLongtitude,
    this.lastLatitude,
    this.lastLongitude});

  factory JsonStruct.fromJson(Map<String, dynamic> parsedJson){
    return JsonStruct(
      curretnLatitude: parsedJson['Clatitude'],
      currentLongtitude : parsedJson['Clongitude'],
      lastLatitude : parsedJson ['Llatitude'],
      lastLongitude : parsedJson ['Llongitude']
    );
  }
}