import 'dart:async';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:navigation_flame/States/mainMenu.dart';
import 'package:navigation_flame/States/splachScreen.dart';
import 'package:navigation_flame/data/data.dart';
import 'package:provider/provider.dart';
import 'locationData/userLocation.dart';


class LocationState  extends StatelessWidget
{


  Data data;
  LocationState(this.data)
  {
    
  }

  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserLocation>(
      create: (context) => LocationService().locationStream,
      child: MaterialApp(title:"location", home: SplashScreenProceedit(data)));
    throw UnimplementedError();
  }

}

class LocationService
{
  UserLocation _currentLocation;
  Location location = Location();
  var userLocation;
  
  //copntinusialy emit location updates

  StreamController<UserLocation> _locationController =
  StreamController<UserLocation>.broadcast();

  Stream<UserLocation> get locationStream  => _locationController.stream;

  LocationService()
  {
    location.requestPermission().then((granted)
    {
     
        location.onLocationChanged.listen((locationData)
        {
          if(locationData !=  null)
          {
            _locationController.add(UserLocation(latitude: locationData.latitude,
                                                longitude: locationData.longitude));
          }
        });
    });
  }

  Future<UserLocation> getLocation() async
  {
    try
    {
      userLocation = await  location.getLocation();
      _currentLocation = UserLocation
      (latitude: userLocation.latitude,
       longitude: userLocation.longitude);
    }catch(e){
      _currentLocation = UserLocation
      (latitude: 0.0,
       longitude: 0.0);
    }
    return _currentLocation;
  }
}